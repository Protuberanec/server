#include "thtcpserver.h"

void tcp_server::SendDataToClient(QTcpSocket *client, const QString &str)
{
    //if anybody connected should be send data to all connected users
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_0);
    out << str;
    out.device()->seek(0);
    client->write(str.toUtf8());//arrBlock);
}

tcp_server::tcp_server()
{
//    count_user = 0;
//    tcpServer = new QTcpServer();
////    connect(tcpServer, &QTcpServer::newConnection, this, &thTcpServer::slotNewUser);
//    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(slotNewUser()));
//    bool error = tcpServer->listen(QHostAddress::LocalHost, DEFAULT_PORT);
//    tcpServer->setMaxPendingConnections(MAX_CLIENT);

//    if (error == false) {
//        qDebug("------------------!!!any problem with server!!!-------------------");
//    }
//    else {
//        qDebug() << QString("---------------------server has started port is %1---------------------").arg(DEFAULT_PORT);
//    }
}


tcp_server::tcp_server(unsigned short num_port)
{
    count_user = 0;
    tcpServer = new QTcpServer();
    tcpServer->setMaxPendingConnections(MAX_CLIENT);
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(slot_NewUser())); //    connect(tcpServer, &QTcpServer::newConnection, this, &thTcpServer::slotNewUser);
    bool error = tcpServer->listen(QHostAddress::Any, num_port);

    if (error == false) {
        qDebug("------------------!!!any problem with server!!!-------------------");
        emit sig_Errors("can not listen...");
    }
    else {
        qDebug() << tr("---------------------server has started port is %1---------------------").arg(num_port);
    }
}

void tcp_server::CreateServer(unsigned short num_port)
{
    if (tcpServer == NULL) {
        return;
    }
    tcpServer = new QTcpServer();
    count_user = 0;

    tcpServer->setMaxPendingConnections(MAX_CLIENT);
    bool error = tcpServer->listen(QHostAddress::Any, num_port);
//    connect(tcpServer, &QTcpServer::newConnection, this, &thTcpServer::slotNewUser);

    if (error == false) {
        qDebug("------------------!!!any problem with server!!!-------------------");
        emit sig_ErrorServerClose();
    }
    else {
        qDebug() << tr("---------------------server has started port is %1---------------------").arg(num_port);
        connect(tcpServer, SIGNAL(newConnection()), this, SLOT(slot_NewUser()));
        connect(tcpServer, &QTcpServer::acceptError, this, &tcp_server::slot_ErrorsProcess);
    }
}

void tcp_server::CloseServer()
{
    for (int i = 0; i < sockets.size(); i++) {
        sockets.at(i)->disconnectFromHost();
    }

    tcpServer->close();
}

void tcp_server::slot_NewUser()
{
    count_user++;
    printf("new user was connected to out server\n");
    QTcpSocket *ClientSocket = tcpServer->nextPendingConnection();
    connect(ClientSocket, SIGNAL(finished()), this, SLOT(deleteLater()));
    connect(ClientSocket, SIGNAL(destroyed()), this, SLOT(deleteLater()));
    connect(ClientSocket, SIGNAL(readyRead()), this, SLOT(slot_ReadData()));

    emit sig_InfoClient(ClientSocket->peerAddress().toString());

    SendDataToClient(ClientSocket, (QString)"ok you have connected!!\n");
    sockets.push_back(ClientSocket);
}

void tcp_server::slot_ReadData()
{
//    qDebug("read data");
    QTcpSocket *ClientSocket = (QTcpSocket *)sender();

    int test_size_client = ClientSocket->bytesAvailable();
    while(test_size_client)
    {
//        printf("size data : %d\n", test_size_client);
        char *temp_data = new char [test_size_client + 1];
        memset(temp_data, 0x00, test_size_client + 1);
        ClientSocket->read(temp_data, test_size_client);
//        printf("data is : %s\n", temp_data);
        emit sig_NewDataFromClient(temp_data); //not sure...

        //here should be process data obtained from server

        delete [] temp_data;
        test_size_client = ClientSocket->bytesAvailable();
    }
}

void tcp_server::slot_SendToClientData(QByteArray data)
{
    //here need to prepare data... and send to user...
    for (int i = 0; i < sockets.size(); i++) {
        QString temp_str = QString("TCP %0").arg(sockets.at(i)->Text);
//        emit sigWriteLog(data, temp_str);
        SendDataToClient(sockets.at(i), data);
    }
}

void tcp_server::slot_ErrorsProcess(QAbstractSocket::SocketError error)
{
    QByteArray error_name;
    switch (error) {
    case QAbstractSocket::SocketAccessError:
        error_name = "SocketAccessError";
        break;
    case QAbstractSocket::SocketTimeoutError :
        error_name = "SocketTimeoutError";
        break;
    case QAbstractSocket::NetworkError :
        error_name = "NetworkError";
        break;
    case QAbstractSocket::OperationError :
        error_name = "OperationError";
    default:
        error_name = tr("unknown error code is %1").arg(error).toUtf8();
        break;

    }

    emit sig_Errors(error_name);


}
