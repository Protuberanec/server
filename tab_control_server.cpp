#include "tab_control_server.h"

void tab_control_server::PlaceComponent()
{
    grid_box = new QGridLayout();
    grid_box->setSpacing(5);
    grid_box->addWidget(LE_StrData, 0, 0);

    grid_box->addWidget(PB_SendData, 0, 1);
    grid_box->addWidget(table_info, 1, 0);
    grid_box->addWidget(errors_log, 2, 0);
    grid_box->setColumnStretch(0, 3);

    control_panel_box = new QVBoxLayout();
    grid_box->addLayout(control_panel_box, 1, 1, 3, 1);
    control_panel_box->addWidget(PB_SendFile);
    control_panel_box->addWidget(info_clients);
//    control_panel_box->addStretch(1);
    control_panel_box->addWidget(PB_CloseServer);
}

void tab_control_server::CreateTableInfo()
{
    table_info = new QTableWidget();
    table_info->setColumnCount(3);
    QStringList header_name;
    header_name << "time" << "data" << "to/from";
    table_info->setHorizontalHeaderLabels(header_name);
}

void tab_control_server::CreateTableError()
{
    errors_log = new QTableWidget();
    errors_log->setColumnCount(2);
    QStringList header_name;
    header_name << "time" << "error name";
    errors_log->setHorizontalHeaderLabels(header_name);
}

void tab_control_server::UpdateTable(QString data, quint16 row, quint16 col, const QColor &col_cell)
{
    QTableWidgetItem *new_item;
    new_item = new QTableWidgetItem(data);
    new_item->setBackground(col_cell);  //    new_item->setBackgroundColor(col_cell);
    table_info->setItem(row, col, new_item);
}

tab_control_server::tab_control_server(QWidget *wdgt) {
    CreateTableInfo();
    CreateTableError();

    PB_SendData = new QPushButton();
    PB_SendData->setText("send data");
    connect(PB_SendData, SIGNAL(clicked()), this, SLOT(slot_ButtonSend()));

    PB_CloseServer = new QPushButton();
    PB_CloseServer->setText("close server");
    connect(PB_CloseServer, SIGNAL(clicked()), this, SLOT(slot_CloseServer()));

    PB_SendFile = new QPushButton();
    PB_SendFile->setText("send file");
    connect(PB_SendFile, SIGNAL(clicked()), this, SLOT(slot_SendToClientFile()));

    info_clients = new QTableWidget();
    info_clients->setColumnCount(2);
    QStringList header_clients;
    header_clients << "time" << "ip";
    info_clients->setHorizontalHeaderLabels(header_clients);


    LE_StrData = new QLineEdit();

    PlaceComponent();

    connect(this, SIGNAL(sig_DataToClient(QByteArray)), this, SLOT(slot_SendToClientData(QByteArray)));
    connect(this, SIGNAL(sig_NewDataFromClient(QByteArray)), this, SLOT(slot_NewDataFromClient(QByteArray)));
    connect(this, SIGNAL(sig_InfoClient(QString)), this, SLOT(slot_AddNewUser(QString)));
    connect(this, SIGNAL(sig_ErrorServerClose()), SIGNAL(sig_CloseServer()));
    connect(this, SIGNAL(sig_Errors(QByteArray)), this, SLOT(slot_ShowErrors(QByteArray)));

    if (wdgt == nullptr)
        return;

    wdgt->setLayout(grid_box);
    wdgt->show();
}

void tab_control_server::slot_ButtonSend()
{
    emit sig_DataToClient(LE_StrData->text().toUtf8());

    QTime TimCur = QTime::currentTime();

    quint16 num_row = tabInfo_AddNewRow();
    UpdateTable(TimCur.toString("HH:mm:ss:zzz"), num_row, 0, QColor(0x9F, 0xFF, 0xFF));
    UpdateTable(LE_StrData->text().toUtf8(), num_row, 1, QColor(0x9F, 0xFF, 0xFF));
    UpdateTable("from", num_row, 2, QColor(0x9F, 0xFF, 0xFF));
}

void tab_control_server::slot_NewDataFromClient(QByteArray client_data)
{
    QTime TimCur = QTime::currentTime();
    quint16 num_row = tabInfo_AddNewRow();
    UpdateTable(TimCur.toString("HH:mm:ss:zzz"), num_row, 0, QColor(0x50, 0xf0, 0x80));
    UpdateTable(client_data.data(), num_row, 1, QColor(0x50, 0xf0, 0x80));
    UpdateTable("to", num_row, 2, QColor(0x50, 0xf0, 0x80));
}

void tab_control_server::slot_CloseServer()
{
    CloseServer();
    emit sig_CloseServer();
}

void tab_control_server::slot_SendToClientFile()
{
    QTime TimCur = QTime::currentTime();
    quint16 num_row = tabInfo_AddNewRow();
    UpdateTable(TimCur.toString("HH:mm:ss:zzz"), num_row, 0, QColor(0x0F0, 0xf0, 0x80));
    UpdateTable("send file", num_row, 1, QColor(0x0F0, 0xf0, 0x80));
    UpdateTable("from", num_row, 2, QColor(0x0F0, 0xf0, 0x80));
}

void tab_control_server::slot_AddNewUser(QString client_data)
{
    info_clients->setRowCount(info_clients->rowCount() + 1);
    quint16 cur_row = info_clients->rowCount() - 1;

    QTime time_connection = QTime::currentTime();

    QTableWidgetItem *new_item;
    new_item = new QTableWidgetItem(time_connection.toString("HH:mm:ss:zzz"));
    info_clients->setItem(cur_row, 0, new_item);

    new_item = new QTableWidgetItem(client_data);
    info_clients->setItem(cur_row, 1, new_item);

}

void tab_control_server::slot_ShowErrors(QByteArray errors_name)
{
    quint16 cur_row = tabError_AddNewRow();
    QTableWidgetItem *new_item;
    QTime time_error = QTime::currentTime();
    new_item = new QTableWidgetItem(time_error.toString("HH:mm:ss:zzz"));
    errors_log->setItem(cur_row, 0, new_item);

    new_item = new QTableWidgetItem(errors_name.data());
    errors_log->setItem(cur_row, 1, new_item);
}





