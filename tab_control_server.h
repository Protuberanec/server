#ifndef TAB_CONTROL_SERVER_H
#define TAB_CONTROL_SERVER_H

#include <QObject>
#include    <QTableWidget>
#include    <QTableWidgetItem>
#include    <QPushButton>
#include    <QLineEdit>
#include    <QGridLayout>
#include    <QVBoxLayout>
#include    <QHBoxLayout>

#include    "thtcpserver.h"


class tab_control_server : public tcp_server
{
    Q_OBJECT
private :
    QTableWidget *table_info;
    QTableWidget *info_clients;
    QTableWidget *errors_log;
    QPushButton *PB_SendData;
    QPushButton *PB_CloseServer;
    QPushButton *PB_SendFile;
    QLineEdit *LE_StrData;
    QGridLayout *grid_box;
    QVBoxLayout *control_panel_box;

    void PlaceComponent();
    void CreateTableInfo();
    void CreateTableError();
    void UpdateTable(QString data, quint16 row, quint16 col, const QColor &col_cell);
    quint16 tabInfo_AddNewRow() {
        table_info->setRowCount(table_info->rowCount() + 1);
        return table_info->rowCount() - 1;
    }

    quint16 tabError_AddNewRow() {
        errors_log->setRowCount(errors_log->rowCount() + 1);
        return errors_log->rowCount() - 1;
    }

public:
    explicit tab_control_server(QWidget *wdgt = nullptr);

signals:
    void sig_DataToClient(QByteArray);
    void sig_CloseServer();
public slots:
    void slot_ButtonSend();
    void slot_NewDataFromClient(QByteArray);
    void slot_CloseServer();

    void slot_SendToClientFile();

    void slot_AddNewUser(QString);
    void slot_ShowErrors(QByteArray error_name);

};

#endif // TAB_CONTROL_SERVER_H

