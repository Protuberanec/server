#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include    <QTableWidgetItem>
#include    <QTime>
#include    <QFile>

#include    <QLineEdit>

#include    "tab_control_server.h"
#include    "thtcpserver.h"


#define AMOUNT_COL  3
#define DEFAULT_SETTINGS_FILE_NAME  "last_settings.ini"

namespace Ui {
class MainWindow;
}

struct SETTINGS {
    quint8 isUpdated;
    quint16 port_num;

    SETTINGS() {
        isUpdated = 0;
    }
};

class MainWindow : public QMainWindow
{
    Q_OBJECT
private :
    QList<tab_control_server*> server_tabs;

    struct SETTINGS current_param;
    QFile last_settings;
    void CreateNewSettingsFile();
    void UpdateSettingsFile();
    void GetParam();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    tcp_server *test_server;
private:
    Ui::MainWindow *ui;

private slots :
    void slot_CreateServer();
    void slot_RemoveTab();

    void slot_PortChanged();
};

#endif // MAINWINDOW_H
