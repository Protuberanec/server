#include "mainwindow.h"
#include "ui_mainwindow.h"

/*
    идея... должен быть файлик, в котором должно быть прописано, что сервер получает и как на это отвечать
    если данных много или они имеют специфический формат, то после принятого сообщение указывается имя файла для
    передачи его содержимого клиенту (серверу) по сути это протокол обмена сообщениями....

    файл с такими указаниями в ручном режиме подгружается в программу...

    Добавить возможность создавать серверы при нажатии кнопки, при этом должна создаваться вкладка
*/

void MainWindow::CreateNewSettingsFile()
{
    last_settings.setFileName(DEFAULT_SETTINGS_FILE_NAME);
    if (last_settings.exists() == false) {
        last_settings.open(QIODevice::WriteOnly);
        QString param = QString("port:%1\n").arg(ui->LE_PortServer->text());
        last_settings.write(param.toUtf8());
        last_settings.close();
    }
}

void MainWindow::UpdateSettingsFile()
{
    if (current_param.isUpdated == 0)
        return;

    last_settings.open(QIODevice::WriteOnly);
    QString param = QString("port:%1\n").arg(ui->LE_PortServer->text());
    last_settings.write(param.toUtf8());
    last_settings.close();
}

void MainWindow::GetParam()
{
    CreateNewSettingsFile();

    last_settings.setFileName(DEFAULT_SETTINGS_FILE_NAME);
    last_settings.open(QIODevice::ReadOnly);

    bool ok = false;
    while(!last_settings.atEnd()) {
        QByteArray param = last_settings.readLine();
        param.resize(param.size() - 1);
        QByteArrayList param_val = param.split(':');
        if (param_val.size() > 1) {
            if (param_val.at(0) == "port") {
                current_param.port_num = param_val.at(1).toUShort(&ok, 10);  //update this param struct
                ui->LE_PortServer->setText(param_val.at(1));

            }
        }
    }

    last_settings.close();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->PB_CreateServer, SIGNAL(clicked()), this, SLOT(slot_CreateServer()));

    GetParam();

    connect(ui->LE_PortServer, &QLineEdit::editingFinished, this, &MainWindow::slot_PortChanged);

}

MainWindow::~MainWindow()
{
    UpdateSettingsFile();
    delete ui;
}

void MainWindow::slot_CreateServer()
{
    QWidget *temp_tab = new QWidget();

    tab_control_server *new_tabs = new tab_control_server(temp_tab);
    server_tabs.append(new_tabs);

    ui->tabWidget->addTab(temp_tab, tr(".01:%1").arg(ui->LE_PortServer->text()));

    connect(server_tabs.last(), SIGNAL(sig_CloseServer()), this, SLOT(slot_RemoveTab()));
    connect(server_tabs.last(), SIGNAL(sig_ErrorServerClose()), this, SLOT(slot_RemoveTab()));

    server_tabs.last()->CreateServer(ui->LE_PortServer->text().toUShort());
}

void MainWindow::slot_RemoveTab()
{
    tab_control_server *pointer = (tab_control_server*)sender();
    int i = 0;
    for (; i < server_tabs.size(); i++) {
        if (server_tabs.at(i) == pointer) {
            ui->tabWidget->removeTab(i);
            delete server_tabs.at(i);
            server_tabs.removeAt(i);
            break;
        }
    }
}

void MainWindow::slot_PortChanged()
{
    if (ui->LE_PortServer->text().toInt() != current_param.port_num)
        current_param.isUpdated = 1;
    else
        current_param.isUpdated = 0;
}

















