#ifndef THTCPSERVER_H
#define THTCPSERVER_H

#include    <QTcpServer>
#include    <QTcpSocket>
#include    <QObject>
#include    <QList>
#include    <QString>
#include    <QDataStream>
#include    <QTime>
#include    <QThread>
#include    <QString>

#define MAX_CLIENT  10
#define DEFAULT_PORT    21021


class tcp_server : public QObject
{
    Q_OBJECT
protected:
    void CloseServer();
private:
    unsigned int count_user;
    QTcpServer *tcpServer;
//    QTcpSocket *firstSocket; // клиент
    QList<QTcpSocket *> sockets; // список получателей данных

    quint16 m_nNextBlockSize;
    void SendDataToClient(QTcpSocket *client, const QString &str);


public:
    tcp_server();
    tcp_server(unsigned short num_port);

    void CreateServer(unsigned short num_port);

signals:
    void sig_NewDataFromClient(QByteArray data);   //signal to send data to client....
    void sig_WriteLog(QString, QString who_send);
    void sig_ErrorServerClose();
    void sig_InfoClient(QString);
    void sig_Errors(QByteArray);

private slots :
    void slot_NewUser();
    void slot_ReadData();
    void slot_SendToClientData(QByteArray data);    //slot for send data to client...
    void slot_ErrorsProcess(QAbstractSocket::SocketError);
};

#endif // THTCPSERVER_H
